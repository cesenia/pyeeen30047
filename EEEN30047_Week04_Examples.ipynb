{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EEEN30047 Power System Analysis: Week 04 - Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2021 Martínez Ceseña — University of Manchester, UK***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides several examples covering frequency regulation and generation control, which use the tools that were developed in the ***EEEN30047 Power System Analysis: Week 04 - Frequency regulation*** notebook.\n",
    "\n",
    "The use of the notebooks is optional and will not be marked. That said, you are strongly encouraged to play with the tools and examples, as such activities will better prepare you for the exams."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Generator and load characteristics](#Generator-and-load-characteristics)\n",
    "- [Coordinating two generators](#Coordinating-two-generators)\n",
    "- [Coordinating area frequency response](#Coordinating-area-frequency-response)\n",
    "- [Area frequency response](#Area-frequency-response)\n",
    "- [Control model](#Control-model)\n",
    "- [Frequency response including control models](#Frequency-response-including-control-models)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-04---Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for EEEN30047 Week 1 - Nodal analysis \n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import interact\n",
    "import ipywidgets as widgets\n",
    "import matplotlib\n",
    "#matplotlib.pyplot.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Requirement already satisfied: nbimporter in c:\\programdata\\anaconda3\\lib\\site-packages (0.3.4)\n"
     ]
    }
   ],
   "source": [
    "!pip install nbimporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nbimporter\n",
    "\n",
    "Week_04 = __import__('EEEN30047_Week04')\n",
    "plot_ΔPm = Week_04.plot_ΔPm\n",
    "get_Primary_Response = Week_04.get_Primary_Response\n",
    "control_model = Week_04.control_model\n",
    "plot_control = Week_04.plot_control"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generator and load characteristics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check how the primary ($R$ and $D$) and secondary ($\\Delta P_{ref}$ and $\\Delta P_L$) frequency regulation characteristics of synchronous generators and loads can be adjusted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f524d1e3aafc436fbdaff4c3c5de05d1",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='ΔPref1 (pu)', max=0.1, min=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib widget    \n",
    "@interact\n",
    "def fast_bfs(ΔPref = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0.0,\n",
    "                                         description='ΔPref1 (pu)', continuous_update=False), \n",
    "             R = widgets.FloatSlider(min=0.01,max=1,step=0.01,value=0.1,\n",
    "                                     description='R1 (pu)', continuous_update=False),\n",
    "             ΔPL = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0,\n",
    "                                       description='ΔPL (pu)', continuous_update=False), \n",
    "             D = widgets.FloatSlider(min=0.01,max=2,step=0.01,value=0.6,\n",
    "                                     description='D (pu)', continuous_update=False),\n",
    "             Δf = widgets.FloatSlider(min=-0.01,max=0.01,step=0.001,value=-0.005,\n",
    "                                      description='Δf (pu)',readout_format='.3f', continuous_update=False)):\n",
    "    ΔPref_List = [ΔPref]\n",
    "    R_List = [R]\n",
    "    ΔPL_List = [ΔPL]\n",
    "    D_List = [D]\n",
    "    Δf_List = [-0.01, 0.01]\n",
    "    ΔPm_List = [-0.2, 0.2]\n",
    "    plot_ΔPm(Δf, ΔPref_List, R_List, ΔPL_List, D_List, ΔPm_List, Δf_List)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Coordinating two generators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Analyse the droop ($R$) and load reference ($\\Delta P_{ref}$) characteristics of synchronous generators:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "eaba16d144794fd8926a1a2a7ffea08d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='ΔPref1 (pu)', max=0.1, min=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib widget    \n",
    "@interact\n",
    "def fast_bfs(ΔPref1 = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0.0,\n",
    "                                          description='ΔPref1 (pu)', continuous_update=False), \n",
    "             R1 = widgets.FloatSlider(min=0.01,max=1,step=0.01,value=0.1,\n",
    "                                      description='R1 (pu)', continuous_update=False),\n",
    "             ΔPref2 = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0.05,\n",
    "                                          description='ΔPref2 (pu)', continuous_update=False), \n",
    "             R2 = widgets.FloatSlider(min=0.01,max=1,step=0.01,value=0.2,\n",
    "                                      description='R2 (pu)', continuous_update=False),\n",
    "             Δf = widgets.FloatSlider(min=-0.01,max=0.01,step=0.001,value=0,\n",
    "                                      description='Δf (pu)',readout_format='.3f', continuous_update=False)):\n",
    "    ΔPref_List = [ΔPref1, ΔPref2]\n",
    "    R_List = [R1, R2]\n",
    "    Δf_List = [-0.01, 0.01]\n",
    "    ΔPm_List = [-0.2, 0.2]\n",
    "    plot_ΔPm(Δf, ΔPref_List, R_List, [], [], ΔPm_List, Δf_List)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Coordinating area frequency response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the figure to analyse the expected response of multiple devices to frequency variations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2b2b083fc65b4c859c68fb24e3041097",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.0, continuous_update=False, description='ΔPref1 (pu)', max=0.1, min=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "#%matplotlib widget    \n",
    "@interact\n",
    "def fast_bfs(ΔPref1 = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0,\n",
    "                                          description='ΔPref1 (pu)', continuous_update=False), \n",
    "             R1 = widgets.FloatSlider(min=0.01,max=1,step=0.01,value=0.1,\n",
    "                                      description='R1 (pu)', continuous_update=False),\n",
    "             ΔPref2 = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0.0,\n",
    "                                          description='ΔPref2 (pu)', continuous_update=False), \n",
    "             R2 = widgets.FloatSlider(min=0.01,max=1,step=0.01,value=0.2,\n",
    "                                      description='R2 (pu)', continuous_update=False),\n",
    "             ΔPL = widgets.FloatSlider(min=-0.1,max=0.1, step=0.01, value=0,\n",
    "                                       description='ΔPL (pu)', continuous_update=False), \n",
    "             D = widgets.FloatSlider(min=0.01,max=2,step=0.01,value=0.6,\n",
    "                                     description='R2 (pu)', continuous_update=False),\n",
    "             Δf = widgets.FloatSlider(min=-0.01,max=0.01,step=0.001,value=0.005,\n",
    "                                      description='Δf (pu)',readout_format='.3f', continuous_update=False)):\n",
    "    ΔPref_List = [ΔPref1, ΔPref2]\n",
    "    R_List = [R1, R2]\n",
    "    ΔPL_List = [ΔPL]\n",
    "    D_List = [D]\n",
    "    Δf_List = [-0.01, 0.01]\n",
    "    ΔPm_List = [-0.2, 0.2]\n",
    "    plot_ΔPm(Δf, ΔPref_List, R_List, ΔPL_List, D_List, ΔPm_List, Δf_List)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Area frequency response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example illustrates, through numerical calculations, the primary and secondary frequency response of an area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "0dfc582693bf4794a8fb93deba759383",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=-100.0, description='Contingency (MW)', max=500.0, min=-500.0, step=1.…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Example01(Cont = widgets.FloatSlider(min=-500,max=500, step=1, value=-100,description='Contingency (MW)'), \n",
    "             Area = widgets.FloatSlider(min=1,max=2,step=1,value=1, description='Area:')):\n",
    "    Dist = {\n",
    "        'Area':Area,\n",
    "        'Magnitude': Cont,  # Positive: Generation is higher than load\n",
    "        'Flow': 0  # From area 1 to 2 by default\n",
    "    }\n",
    "    Gen = [\n",
    "        {'Area':1, 'Capacity': 4000, 'R':4, 'Units': '%', 'Output': 500},\n",
    "        {'Area':1, 'Capacity': 2000, 'R':0.00125, 'Units': 'Hz/MW', 'Output': 500}\n",
    "    ]\n",
    "    Load = [\n",
    "        {'D':0.6, 'Units': 'pu'},\n",
    "    ]\n",
    "    Base = 4000\n",
    "    F = 50\n",
    "\n",
    "    get_Primary_Response(Dist, Gen, Load, Base, F)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Control model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use this example to explore the impacts of the different settings used for the generator, load, prime mover, droop control and governon.  \n",
    "- $ΔP_L$: Sudden change to the load-generation balance (pu)\n",
    "- $R$: Regulation constant (pu)\n",
    "- $D$: Damping constant (pu)\n",
    "- $M$: Angular momentum (pu)\n",
    "- $T_{ch}$: Charging time (pu)\n",
    "- $K_g$: Gain (pu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f6e3e568b9f543168dd529a2ada112d1",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=-0.1, continuous_update=False, description='ΔPL (pu)', max=0.5, min=-0…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Control_Example(ΔPL = widgets.FloatSlider(min=-0.5,max=0.5,step=0.001,value=-0.1,\n",
    "                                              description='ΔPL (pu)', readout_format='.3f', continuous_update=False),\n",
    "                    R = widgets.FloatSlider(min=0.01,max=0.5, step=0.01, value=0.04,\n",
    "                                            description='R (pu)', continuous_update=False),\n",
    "                    D = widgets.FloatSlider(min=0.01,max=5.0, step=0.01, value=2,\n",
    "                                            description='D (pu)', continuous_update=False),\n",
    "                    M = widgets.FloatSlider(min=0,max=20, step=0.1, value=10,\n",
    "                                            description='M (pu)', continuous_update=False),\n",
    "                    Tch = widgets.FloatSlider(min=0.0,max=1, step=0.01, value=0.3,\n",
    "                                              description='Tch (pu)', continuous_update=False),\n",
    "                    Kg = widgets.FloatSlider(min=0.1,max=30, step=0.1, value=20,\n",
    "                                            description='Kg (pu)', continuous_update=False)):\n",
    "    T = 30\n",
    "    Tg = 1/Kg/R\n",
    "    t, Δw = control_model(ΔPL, R, D, M, Tch, Tg, T)\n",
    "    plot_control(Δw, t)\n",
    "    print('\\nΔw after %.4f seconds: %.4f'%(T, Δw[-1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Frequency response including control models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This last example bring together the steady-state and dynamic calculations presented above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fa497a56dac14828b582453b1c161127",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=-100.0, continuous_update=False, description='Contingency (MW)', max=5…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Example01(Cont = widgets.FloatSlider(min=-500,max=500, step=1, value=-100,\n",
    "                                         description='Contingency (MW)', continuous_update=False),\n",
    "              R = widgets.FloatSlider(min=0.01,max=0.1, step=0.01, value=0.04,\n",
    "                                      description='R (pu)', continuous_update=False),\n",
    "              D = widgets.FloatSlider(min=0.01,max=5.0, step=0.01, value=2,\n",
    "                                      description='D (pu)', continuous_update=False)):\n",
    "    Dist = {\n",
    "        'Area': 1,\n",
    "        'Magnitude': Cont,  # Positive: Generation is higher than load\n",
    "        'Flow': 0  # From area 1 to 2 by default\n",
    "    }\n",
    "    Gen = [\n",
    "        {'Area':1, 'Capacity': 1000, 'R':R, 'Units': 'pu', 'Output': 500, 'ΔPref':0}\n",
    "    ]\n",
    "    Load = [\n",
    "        {'D':D, 'Units': 'pu'}\n",
    "    ]\n",
    "    Base = 1000\n",
    "    F = 50\n",
    "\n",
    "    get_Primary_Response(Dist, Gen, Load, Base, F)\n",
    "    \n",
    "    M = 10\n",
    "    Tch = 0.3\n",
    "    Tg = 1/20/R\n",
    "    T = 50\n",
    "    ΔPL = Cont/Base\n",
    "    t, Δw = control_model(ΔPL, R, D, M, Tch, Tg, T)\n",
    "    plot_control(Δw, t)\n",
    "    print('\\nΔw after %.4f seconds: %.4f'%(T, Δw[-1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-04---Examples)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "280.208px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
