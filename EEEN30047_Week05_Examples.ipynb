{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EEEN30047 Power System Analysis: Week 05 - Examples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2021 Martínez Ceseña — University of Manchester, UK***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook is dedicated to providing examples covering frequency regulation and generation control. The specific tools presented here have been developed in other notebooks, expecially in `EEEN30047_Week05`.\n",
    "\n",
    "The use of the notebooks is optional and will not be marked. That said, you are strongly encouraged to play with the tools and examples, as such activities will better prepare you for the exams."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Accuracy of the DC power flow](#Accuracy-of-the-DC-power-flow)\n",
    "- [Primary frequency regulation](#Primary-frequency-regulation)\n",
    "- [Secondary frequency regulation](#Secondary-frequency-regulation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for EEEN30047 Week 4 - Frequency regulation \n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk\n",
    "\n",
    "This notebook builds on the materials that were presented during the last week. Therefore, before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for EEEN30047:\n",
    "  - Week 4 - Frequency regulation\n",
    "  - Week 5 - Interconnected systems\n",
    "\n",
    "Also, the interconnected models are based on a simplified approach to model power flows. Therefore, it is also recommended to review our previous lectures on power flow analysis:\n",
    "  - Week 1 - Nodal analysis \n",
    "  - Week 2 - Power Flow Formulation \n",
    "  - Week 3 - Newton Raphson\n",
    "\n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The notebook borrows several tools developed in previous weeks, so we need to import them here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Requirement already satisfied: nbimporter in c:\\programdata\\anaconda3\\lib\\site-packages (0.3.4)\n"
     ]
    }
   ],
   "source": [
    "!pip install nbimporter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nbimporter\n",
    "\n",
    "Week_01 = __import__('EEEN30047_Week01')\n",
    "get_Ybus = Week_01.get_Ybus\n",
    "\n",
    "Week_02 = __import__('EEEN30047_Week02')\n",
    "get_Bus_Type = Week_02.get_Bus_Type\n",
    "develop_PF_Equations = Week_02.develop_PF_Equations\n",
    "\n",
    "Week_03 = __import__('EEEN30047_Week03')\n",
    "Newtons_Method = Week_03.Newtons_Method\n",
    "Visualize_Elec = Week_03.Visualize_Elec\n",
    "\n",
    "Week_04 = __import__('EEEN30047_Week04')\n",
    "get_Primary_Response = Week_04.get_Primary_Response\n",
    "get_pu = Week_04.get_pu\n",
    "get_Δw = Week_04.get_Δw\n",
    "get_step = Week_04.get_step\n",
    "\n",
    "Week_05 = __import__('EEEN30047_Week05')\n",
    "run_DCPF = Week_05.run_DCPF\n",
    "Visualize_DC = Week_05.Visualize_DC\n",
    "get_Tie_Data = Week_05.get_Tie_Data\n",
    "get_TieLine = Week_05.get_TieLine\n",
    "print_TieLine = Week_05.print_TieLine\n",
    "interconnected_frequency_primary = Week_05.interconnected_frequency_primary\n",
    "plot_Δw = Week_05.plot_Δw\n",
    "interconnected_power = Week_05.interconnected_power\n",
    "plot_Ppu = Week_05.plot_Ppu\n",
    "interconnected_model = Week_05.interconnected_model\n",
    "interconnected_frequency_secondary = Week_05.interconnected_frequency_secondary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, the following general python libraries are needed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-05---Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accuracy of the DC power flow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The DC model is generally accurate for the analysis of transmission networks where voltages are close to 1 pu and the line resistances are negligible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"Figures/Week02_3Bus_Empty.png\" alt=\"Fig01\" class=\"bg-primary\" width=\"500px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check how the accuracy of the model changes if you increase the line resistances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f9f3e7d2cd564a309adf246e1a81393d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.5, description='P2 (pu)', max=5.0), FloatSlider(value=0.4, descripti…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(P2 = widgets.FloatSlider(min=0.0,max=5,step=0.1,value=1.5,description='P2 (pu)'),\n",
    "         Q2 = widgets.FloatSlider(min=0.0,max=5,step=0.1,value=0.4,description='Q2 (pu)'),\n",
    "         R12 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.0,description='R1,2 (pu)'),\n",
    "         X12 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.01,description='X1,2 (pu)'),\n",
    "         R13 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.0,description='R1,3 (pu)'),\n",
    "         X13 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.02,description='X1,3 (pu)'),\n",
    "         R23 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.0,description='R2,3 (pu)'),\n",
    "         X23 = widgets.FloatSlider(min=0.0,max=0.05,step=0.01,value=0.03,description='X2,3 (pu)')):\n",
    "\n",
    "    Connectivity = [\n",
    "        [1, 2, complex(R12, X12)],\n",
    "        [1, 3, complex(R13, X13)],\n",
    "        [2, 3, complex(R23, X23)]\n",
    "    ]\n",
    "    Load = [\n",
    "        [2, complex(P2, Q2)]\n",
    "    ]\n",
    "    Generator = [\n",
    "        {'Bus':1, 'V':1, '𝜃':0 },\n",
    "        {'Bus':3, 'P':1, 'Q':0.25}\n",
    "    ]\n",
    "\n",
    "    Succes, 𝜃, P = run_DCPF(Connectivity, Load, Generator)\n",
    "    Visualize_DC(Connectivity, 𝜃, P, Succes)\n",
    "\n",
    "    # From Week 01\n",
    "    Ybus = get_Ybus(Connectivity, True, False)\n",
    "\n",
    "    # From Week 02\n",
    "    P_Data, Q_Data = develop_PF_Equations(Load, Generator, Ybus, True, False)\n",
    "    Bus_Data, Bus_Type = get_Bus_Type(Ybus, Load, Generator)\n",
    "\n",
    "    # From Week 03\n",
    "    V_All, 𝜃_All, Threshold, Succes = Newtons_Method(P_Data, Q_Data, Bus_Data, Bus_Type, Generator, 0)\n",
    "    Visualize_Elec(Connectivity, V_All, 𝜃_All, Succes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-05---Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Primary frequency regulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the context of primary frequency regulation:\n",
    "> What happens when the contingency occurs in different areas?\n",
    "\n",
    "> What are the impacts of different contingencies?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c8deb67fafd64eab8c4eef855c84cd48",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.0, description='Area', max=2.0, min=1.0, step=1.0), FloatSlider(valu…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(Area = widgets.FloatSlider(min=1,max=2,step=1,value=1),\n",
    "         Contingency = widgets.FloatSlider(min=-500,max=500,step=5,value=100,\n",
    "                                           description='ΔP (MW)', continuous_update=False)):\n",
    "    Dist = {\n",
    "        'Area': Area,\n",
    "        'Magnitude': Contingency,  # Positive: Generation is higher than load\n",
    "        'Flow': 0  # From area 1 to 2 by default\n",
    "    }\n",
    "    Gen = [\n",
    "        {'Area':1, 'Capacity': 1000, 'R':0.0500, 'Units': 'pu', 'Output': 500},\n",
    "        {'Area':2, 'Capacity': 1000, 'R':0.0625, 'Units': 'pu', 'Output': 500}\n",
    "    ]\n",
    "    Load = [\n",
    "        {'D':0.6, 'Units': 'pu'},\n",
    "        {'D':0.9, 'Units': 'pu'}\n",
    "    ]\n",
    "    Base = 1000\n",
    "    F = 50\n",
    "    T = 30\n",
    "\n",
    "    # System data\n",
    "    M = [10, 6]\n",
    "    Tch = [0.5, 0.6]\n",
    "    Kg = [100, 53.3333]\n",
    "    Tl = 2\n",
    "\n",
    "    Disturbance, Generators, Damping = get_pu(Dist, Gen, Load, Base, F)\n",
    "    Δw, _, _ = get_Δw(Disturbance, Generators, Damping, F)\n",
    "\n",
    "    # Print tie line\n",
    "    R, D = get_Tie_Data(Dist, Generators, Damping, Kg)\n",
    "    Tg = [1/Kg[0]/R[0], 1/Kg[1]/R[1]]\n",
    "    ΔPtie, ΔPtie_pu, Ptie, a1, a2 = get_TieLine(Δw, Disturbance, Generators, Damping, Base)\n",
    "    print_TieLine(ΔPtie, ΔPtie_pu, Ptie, a1, a2)\n",
    "\n",
    "    # Create input signals\n",
    "    t = numpy.linspace(0,T,1000)\n",
    "    ΔPL = get_step(t, 1, Disturbance['Magnitude'])\n",
    "\n",
    "    # Model frequency response\n",
    "    Δw1, Δw2, mod = interconnected_frequency_primary(Dist['Area'], R, D, M, Tch, Kg, Tg, Tl, t, ΔPL)\n",
    "\n",
    "    # Model power response\n",
    "    Tie, P1, P2 = interconnected_power(mod, t, Δw1, Δw2)\n",
    "\n",
    "    # Plot frequency and power response\n",
    "    if Area == 1:\n",
    "        plot_Δw(t, Δw1, Δw2)\n",
    "        plot_Ppu(t, Tie, P1, P2)\n",
    "        print('Δw: %.4f pu after %d seconds'%(Δw1[-1], T))\n",
    "    else:\n",
    "        plot_Δw(t, Δw2, Δw1)\n",
    "        plot_Ppu(t, Tie, P2, P1)\n",
    "        print('Δw: %.4f pu after %d seconds'%(Δw2[-1], T))\n",
    "    print('Tie: %.4f pu after %d seconds'%(Tie[-1], T))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-05---Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Compare the steady-state and dynamic results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "11e2d04b5e5344b786ea983939db188c",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.0, description='Area', max=2.0, min=1.0, step=1.0), FloatSlider(valu…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(Area = widgets.FloatSlider(min=1,max=2,step=1,value=1),\n",
    "         Contingency = widgets.FloatSlider(min=-500,max=500,step=5,value=100,\n",
    "                                           description='ΔP (MW)', continuous_update=False)):\n",
    "    Dist = {\n",
    "        'Area': Area,\n",
    "        'Magnitude': Contingency,  # Positive: Generation is higher than load\n",
    "        'Flow': 0  # From area 1 to 2 by default\n",
    "    }\n",
    "    Gen = [\n",
    "        {'Area':1, 'Capacity': 1000, 'R':0.0500, 'Units': 'pu', 'Output': 500},\n",
    "        {'Area':2, 'Capacity': 1000, 'R':0.0625, 'Units': 'pu', 'Output': 500}\n",
    "    ]\n",
    "    Load = [\n",
    "        {'D':0.6, 'Units': 'pu'},\n",
    "        {'D':0.9, 'Units': 'pu'}\n",
    "    ]\n",
    "    Base = 1000\n",
    "    F = 50\n",
    "    T = 30\n",
    "    get_Primary_Response(Dist, Gen, Load, Base, F)\n",
    "\n",
    "    # System data\n",
    "    M = [10, 6]\n",
    "    Tch = [0.5, 0.6]\n",
    "    Kg = [100, 53.3333]\n",
    "    Tl = 2\n",
    "\n",
    "    Disturbance, Generators, Damping = get_pu(Dist, Gen, Load, Base, F)\n",
    "    Δw, _, _ = get_Δw(Disturbance, Generators, Damping, F)\n",
    "\n",
    "    # Print tie line\n",
    "    R, D = get_Tie_Data(Dist, Generators, Damping, Kg)\n",
    "    Tg = [1/Kg[0]/R[0], 1/Kg[1]/R[1]]\n",
    "    ΔPtie, ΔPtie_pu, Ptie, a1, a2 = get_TieLine(Δw, Disturbance, Generators, Damping, Base)\n",
    "    print_TieLine(ΔPtie, ΔPtie_pu, Ptie, a1, a2)\n",
    "\n",
    "    # Create input signals\n",
    "    t = numpy.linspace(0,T,1000)\n",
    "    ΔPL = get_step(t, 1, Disturbance['Magnitude'])\n",
    "\n",
    "    # Model frequency response\n",
    "    Δw1, Δw2, mod = interconnected_frequency_primary(Dist['Area'], R, D, M, Tch, Kg, Tg, Tl, t, ΔPL)\n",
    "\n",
    "    # Model power response\n",
    "    Tie, P1, P2 = interconnected_power(mod, t, Δw1, Δw2)\n",
    "\n",
    "    # Plot frequency and power response\n",
    "    if Area == 1:\n",
    "        plot_Δw(t, Δw1, Δw2)\n",
    "        plot_Ppu(t, Tie, P1, P2)\n",
    "        print('Δw: %.4f pu after %d seconds'%(Δw1[-1], T))\n",
    "    else:\n",
    "        plot_Δw(t, Δw2, Δw1)\n",
    "        plot_Ppu(t, Tie, P2, P1)\n",
    "        print('Δw: %.4f pu after %d seconds'%(Δw2[-1], T))\n",
    "    print('Tie: %.4f pu after %d seconds'%(Tie[-1], T))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-05---Examples)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Secondary frequency regulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the context of secondary frequency regulation:\n",
    "> What happens when the contingency occurs in different areas?\n",
    "\n",
    "> What are the impacts of different contingencies?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2f6eb820e4ad43f2af87736a1db233e7",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.0, description='Area', max=2.0, min=1.0, step=1.0), FloatSlider(valu…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Bus3(Area = widgets.FloatSlider(min=1,max=2,step=1,value=1),\n",
    "         Contingency = widgets.FloatSlider(min=-500,max=500,step=5,value=100,\n",
    "                                           description='ΔP (MW)', continuous_update=False)):\n",
    "    R = [0.05, 0.0625]\n",
    "    Beta = [20.6, 16.9]\n",
    "    D = [0.6, 0.9]\n",
    "    M = [10, 6]\n",
    "    Tch = [0.5, 0.6]\n",
    "    Kt = [0.3, 0.3]\n",
    "    Kg = [100, 53.3333]\n",
    "    Tg = [1/Kg[0]/R[0], 1/Kg[1]/R[1]]\n",
    "    Tl = 2\n",
    "\n",
    "    Base = 1000\n",
    "    ΔPL = Contingency/Base\n",
    "    T = 30\n",
    "    mod = interconnected_model(R, D, M, Tch, Kg, Tg, Tl, Beta, Kt)\n",
    "    t, Δw1, Δw2 = interconnected_frequency_secondary(mod, Area, ΔPL, T)\n",
    "    Tie, P1, P2 = interconnected_power(mod, t, Δw1, Δw2)\n",
    "    if Area == 1:\n",
    "        plot_Δw(t, Δw1, Δw2)\n",
    "        plot_Ppu(t, Tie, P1, P2)\n",
    "    else:\n",
    "        plot_Δw(t, Δw2, Δw1)\n",
    "        plot_Ppu(t, Tie, P2, P1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30047-Power-System-Analysis:-Week-05---Examples)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "259.323px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
